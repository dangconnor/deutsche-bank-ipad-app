//Directories
var src			= './src',
	jsSrc		= src + '/js/app.js',
	jsDest		= './build/js',
    cssDest     = './build/css';

// Gulp
var gulp 		= require('gulp');

// Gulp plugins
var sass 		= require('gulp-sass'),
	notify 		= require('gulp-notify'),
	uglify 		= require('gulp-uglify'),
    cssmin      = require('gulp-cssmin'),
	rename 		= require('gulp-rename'),
    imagemin    = require('gulp-imagemin'),
    jshint       = require('gulp-jshint'),
    nunjucksRender = require('gulp-nunjucks-render');

// Non Gulp plugins
var browserify  = require('browserify'),
    source      = require('vinyl-source-stream');

// JS Lint
gulp.task('lint', function() {
    return gulp.src(jsSrc)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Sass
gulp.task('styles', function() {

	var sassOptions = {
        errLogToConsole: true,
        outputStyle: 'compact'
    }

    return gulp.src(src + '/sass/*.scss')
    .pipe(sass(sassOptions))
    .on('error', handleErrors)
    .pipe(gulp.dest('./build/css/'));

});

// Browserify
gulp.task('browserify', function() {
    return browserify(jsSrc)
    .bundle()
    .pipe(source('app.js'))
    .on('error', function(err){
      // print the error (can replace with gulp-util)
      console.log(err.message);
      // end this stream
      this.emit('end');
    })
    .pipe(gulp.dest(jsDest));
});

// Minify JS
gulp.task('compressjs', ['browserify'], function() {
  return gulp.src(jsDest + '/app.js')
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(jsDest));
});

// Minify CSS
gulp.task('compresscss', ['styles'], function () {
    gulp.src(cssDest + '/style.css')
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(cssDest));
});

// JSON
gulp.task('json', function () {
    gulp.src(src + '/js/app-data.json')
    .pipe(gulp.dest(jsDest));
});

// Watch task
gulp.task('watch',function() {
    gulp.watch(src + '/sass/**/*.scss',['styles', 'compresscss']);
    gulp.watch(src + '/js/*.js', ['browserify', 'compressjs', 'lint']);
    gulp.watch([src + '/pages/**/*.+(html|nunjucks)', src + '/templates/**/*.+(html|nunjucks)'], ['nunjucks']);
});

// Notify of any errors
var handleErrors = function() {
    var args = Array.prototype.slice.call(arguments);
    notify.onError({
        title: "Compile Error",
        message: "<%= error %>"
    }).apply(this, args);
    this.emit('end'); 
};

// Templating
gulp.task('nunjucks', function() {
    // Gets .html and .nunjucks files in pages
    return gulp.src('src/pages/**/*.+(html|nunjucks)')
    // Renders template with nunjucks
    .pipe(nunjucksRender({
      path: ['src/templates']
    }))
    // output files in root folder
    .pipe(gulp.dest(''))
});

// Image optimisation
gulp.task('images', function() {
    gulp.src(src + '/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('img'))
});

// Build Task
gulp.task('default', ['json', 'browserify', 'styles', 'lint', 'compresscss', 'compressjs', 'nunjucks', 'watch']);
gulp.task('test', ['nunjucks']);