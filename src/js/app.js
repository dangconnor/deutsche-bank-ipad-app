/* global alert */
/* global window */
/* jshint node: true */
/* jshint -W097 */

"use strict";

// Require dependencies
var $ = require('jquery');
var dragula = require('dragula');
require('smoothstate');



/* Application Module */
var c, s, APP = {

    settings: {
        
        /* The application settings and DOM elements */
        Choices: 3,
        ChoicesArr: [],
        ModelsArr: [],
        Components: '.component',
        Data: {},
        DataURL: "build/js/app-data.json",
        ErrorChoices: "Sorry, that combination of choices is not valid. Please change your selection.",
        ErrorModel: "Sorry, we cannot find the associated model at this time. Please change your selection.",
        LoadModelLink: '.js-load-model',
        Main: $('#main'),
        Related: ["e", "f", "g"],
        ResetLink: '.js-reset',
        ModelGenerator: '#model__generator',
        Issues: '.issues',
        FormSent: '.formSent',
        OtherOption: '#otheroption',      
        WufooHash: 'zr3wu5i0euaz8a',
        whatModel:'Not yet chosen',
        modelResultNumber:'Not yet chosen',
        otherProducts: [],
        whatsTheDifference: [],
        amendedWhatsTheDifference: [],
        benefitsArray: [],
        benefitsNotInModel: false
    },

    css: {

        /* CSS classes */
        Containers: 'js-drag-container',
        Disabled: 'options__item--disabled',
        VisualHide: 'visual-hide',
        LinkDisabled: 'js-disabled'

    },

    Init: function () {

        /* Map settings */
        s = this.settings;
        c = this.css;
        
        /* Run init functions */
        APP.Router();
        APP.GetData();
        APP.BuildDragDrop();        
        APP.BindUI();

        if($(s.ModelGenerator).length) {
           APP.PrepareForm();
        }  

        if($(s.Issues).length){
           APP.ClearModels();
        }

        var idleSeconds = 5;

        $(function(){
          var idleTimer;
          function resetTimer(){
            clearTimeout(idleTimer);
            idleTimer = setTimeout(whenUserIdle,idleSeconds*1000);
          }
          $(document.body).bind('touchstart mousemove keydown click',resetTimer); 
          resetTimer(); 
        });

        function whenUserIdle(){

             console.log("To enable User Timeout look for function whenUserIdle()");

            /* UNCOMMENT THIS TO LOG USER OUT AFTER 30 secs (or whatever seconds you set with var idleSeconds above) */
            /*
            sessionStorage.clear(); 
            window.location.href = "/";
            */
        }

        APP.DisableIssues();
        APP.updateDev();
    },

    AddTo: function(val) {

        /* Add the current data value into the array */
        s.ChoicesArr.push(val);

        APP.updateDev();

        /* Disable the original option */
        $('.options').find('[data-option='+ val +']').addClass(c.Disabled);

        /* Extra check to change visual style of related options */
        $.each(s.Related, function(index, data) {
            if($.inArray(data, s.ChoicesArr) != -1) {
                $('.options').find('.options__item--set').addClass(c.Disabled);
                return false;
            } else {
                $('.options').find('.options__item--set').removeClass(c.Disabled);
            }
        });


        /* Check if the what remaining options are compatible with the already selected */

        var compatibleChoices = [];


        for(var aResult=0; aResult<s.Data.results.length; aResult++){

                if( (s.Data.results[aResult].result.indexOf(val) == -1) && (sessionStorage.getItem("userPath") == s.Data.results[aResult].path) ){

                    var numberOfChoices = s.Data.results[aResult].result.length;

                    for(var thisChoice=0; thisChoice<numberOfChoices; thisChoice++){
                            if( (compatibleChoices.indexOf(s.Data.results[aResult].result[thisChoice]) == -1)&&(s.Data.results[aResult].result[thisChoice] != val) ){
                                compatibleChoices.push(s.Data.results[aResult].result[thisChoice]);
                            }
                        }
                    }
            }

            console.log("compatibleChoices = "+compatibleChoices);
    },

    ArrayDifference : function (a1, a2) {

        var a = [], diff = [];

        for (var i = 0; i < a1.length; i++) {
            a[a1[i]] = true;
        }

        for (var i = 0; i < a2.length; i++) {
            if (a[a2[i]]) {
                delete a[a2[i]];
            } else {
                a[a2[i]] = true;
            }
        }

        for (var k in a) {
            if (s.Related.indexOf(k) == -1) {
                diff.push(k); 
            }       
        }

        return diff;
    },

    BindUI: function(){

        /* Prevent default action until app is ready */
        $('.js-disabled').bind('click', false);

        /* Reset Link */
        $('.js-reset').on('click', function(){
            APP.ClearChoices();
        });

        $('.js-launch-lightbox').on('click', function(e){

        if ((sessionStorage.getItem("hasRegistered") !== null)) {
               window.location.href = "../finale.html";            
           } else {
                var target = $(this).attr('href');

                if ($(target).hasClass(c.VisualHide) == true) { 
                    document.getElementsByTagName("h1")[0].innerHTML = "How can you benefit?";
                } else {
                    document.getElementsByTagName("h1")[0].innerHTML = "Where is your solution available"; 
                }

                $(target).toggleClass(c.VisualHide);
                $('.app__nav__item--next').toggleClass(c.LinkDisabled);
                e.preventDefault();

            }

        });


        // Customise page - Store selection, toggle js-disabled

        if( ($(".customise").length)&&(sessionStorage.getItem("userPath") !== null)){
            $(".app__nav__item--next").removeClass("js-disabled").unbind('click', false);     
        }

        $(".customise--radio").on('click', function(e){
            if($(".app__nav__item--next").hasClass("js-disabled")){
                $(".app__nav__item--next").removeClass("js-disabled").unbind('click', false);  
            }
            sessionStorage.setItem('areaOfBusiness',$(this).val());
            sessionStorage.setItem('userPath',$(this).data("path"));  
            APP.updateDev();
        });

        if($(".map_partial").length){
              $(".app__nav__item--next").on('click', function(){  
                    $(".map_modal").css('display', 'none');
                });
        }

        if($(".customise").length){

            $("#customise--button, .js-send-form, .app__nav__item--next").on('click', function(e){  

                e.preventDefault();  

                if(sessionStorage.getItem("userPath") !== null){
                    window.location.href = "issues.html";
                }



            });
        }

        $("#btn-another-model").on('click', function(e){      
            window.location.href = "issues.html";
        });

         $("#btn-new-user, .btn--newuser").click(function(event){    
            event.preventDefault();
            APP.ClearChoices();
            sessionStorage.clear(); 
            window.location.href = "/";
        });   


        $('.btn--reset').on('click', function(){
            APP.ClearChoices();
        });

        $(".modal___more_info").click(function(){   
            var el = $(this);
            var whatToOpen = el.data("whichmodal"); 
            $(whatToOpen).css( { "left" : "0", "opacity" : "1" });
        });


        $(".modal_close").on('click', function(e){  
            var whatToClose = $(this).closest(".a_modal");   
            whatToClose.css("opacity", "0");
            whatToClose
                .delay(1000)
                .queue(function (next) { 
                $(this).css('left', '9999px'); 
                next();               
            });
        });

        $(".map__info li a").click(function(e){  
            e.preventDefault(); 
            var whichCountry = $(this).text();
            var whatColour = $(this).parent("li").attr("class");
            $(".map_partial").css('display', 'none');
            $(".map_modal").css('left', '9999px'); 
            $(".map_modal h2").removeClass();

            if(whatColour == "mydy"){
                $(".map_modal h2").addClass("dblue");
            }else if (whatColour == "mydn"){
                $(".map_modal h2").addClass("lblue");
            }

            $(".map_partial").each(function(){
                if($(this).data("country")==whichCountry){
                    $(this).css('display', 'block');
                }
            });

            $(".map_modal").css( { "left" : "0", "opacity" : "1" });
        });

        $(".map_modal_close").on('click', function(e){  
            $(".map_partial").css('display', 'none');
            $(".map_modal").css('left', '9999px'); 
        });


    },

    BuildDragDrop: function(){

        /* Configure the drag and drop interface */
        var drag = dragula({
            isContainer: function (el) {
                return el.classList.contains(c.Containers);
            },
            accepts: function (el, target, source, sibling) {
                return $(target).hasClass('drop-zone__item') && !$(source).hasClass('drop-zone__item');
            },
            invalid: function (el, handle) {
                return $(el).hasClass(c.Disabled);
            },
            copy: true
        });
        
        /* Listen for on drop event */
        drag.on('drop', function (el, target, source, sibling) {

            /* If dropped into target zone... */
            if(target){

                /* Show the reset link */
                $(s.ResetLink).removeClass(c.LinkDisabled).unbind('click', false);

                /* First check if option is being replaced */
                if($(el).siblings().length){

                    var actualSibling = $(el).siblings();
                    var orig = actualSibling.data('option');

                    actualSibling.remove();
                    APP.RemoveFrom(orig);
                }

                /* Get the current option */
                var option = $(el).data('option');

                /* Run add data routine */
                APP.AddTo(option);
                
                /* Run check status routine */
                APP.CheckStatus();

            }
            
        });

        if ( ($(".issues").length) && (sessionStorage.userPath == 2) ) {
           $(".options__item").each(function(){
                if (s.Related.indexOf($(this).data("option")) != -1){
                    var thisNumber = $(this).find(".module__title").text();
                    thisNumber = thisNumber.split("(").pop();
                    $(this).find(".module__title").text("Market Visibility ("+thisNumber);                              
                }

            });
        }

    },

    BuildModel: function(model) {

        /* Filter the array to return the correct model */
        var result = $.grep(s.Data.models, function(e) {
            return e.model == model;
        });

        if (result.length === 0) {
            
            /* No matching model */
            alert(s.ErrorModel);

        } else if (result.length === 1) {
            
            /* Match found, display the model... */
            /* Enable link, set target, and change visual style of submit buttons */
            $(s.LoadModelLink).unbind('click', false);
            $(s.LoadModelLink).attr('href', result[0].href);
            $(s.LoadModelLink).removeClass(c.LinkDisabled);
            s.whatModel = model;

        } else {
            
            /* Duplicate model found */
            alert(s.ErrorModel);

        }
        
    },


    CheckStatus: function() {
        /* If we have filled all available spaces... */
        if (s.ChoicesArr.length === s.Choices) {
            /* Run routine to determine which model to display */
            APP.FindModel(s.ChoicesArr);            
        }
    },

    ClearChoices: function(){
		s.ChoicesArr = [];
        APP.DisableIssues();
    },

    ClearModels : function(){
        sessionStorage.removeItem("hasRegistered");  
        sessionStorage.removeItem("storedModelTitle");  
        sessionStorage.removeItem("storedModelChoices");
        sessionStorage.removeItem("stroredModelsArray");  
    },

    DisableIssues : function(){

        var userPath;

        if (sessionStorage.getItem("userPath") === null) {
            userPath = 2;
        }else{
            userPath = sessionStorage.getItem("userPath");
        }

        if(userPath == 1){     
            $("div").find("[data-option='j']").addClass(c.Disabled);
            $("div").find("[data-option='i']").addClass(c.Disabled);
        }else if (userPath == 3){
            $("div").find("[data-option='a']").addClass(c.Disabled);
           // $("div").find("[data-option='i']").addClass(c.Disabled);
        }
    },

    FindModel: function(arr){

        /* Set flag for if combination of choices exist */
        var isValid = false;

        /* Loop through each available result set and stop as soon as a match is found */
        $.each(s.Data.results, function(index, data) {

            /* Check user path, as the same matches can lead to different models depending on user path */

            if( ($(data.result).not(arr).length === 0 && $(arr).not(data.result).length === 0) && ($(data.path)[0]==sessionStorage.userPath) ) {
                s.modelResultNumber = data.resultnumber;
                s.otherProducts = data.otherproducts;
                APP.BuildModel(data.model);
                isValid = true;
                return false;
            }
            
        });

        /* If not valid display message */
        if(!isValid){
            alert(s.ErrorChoices);
        }
        
    },

    GetData: function(){

        $.getJSON(s.DataURL, function(data) {
            s.Data = data;
        });

    },

    PrepareForm: function() {

        /* Holder for choices that will populate hidden field */

                var modelTitle, modelChoices = "";


                sessionStorage.removeItem("whatsTheDifferenceStored");  
                sessionStorage.removeItem("benefitsArrayStored");  

                if ((sessionStorage.getItem("storedModelTitle") !== null)) {
                    modelTitle = sessionStorage.getItem("storedModelTitle");
                }

                if ((sessionStorage.getItem("storedModelChoices") !== null)) {
                    modelChoices = sessionStorage.getItem("storedModelChoices");
                }



        /* Collect the form name from the page title */

        /*
            if($('h1').length){ 
                modelTitle = $('h1').text(); 
            } else {
                modelTitle = "No title selected"; 
            }
        */

        if($(".model__title").length){ 
            modelTitle = $(".model__title").text(); 
        } else {
            modelTitle = "No title selected"; 
        }
       
        /* If choices have been selected */
        if(s.ChoicesArr.length){

            s.ModelsArr = [];

            // Loop through "results" to find all of the potential "choices" which belong to this particular "model"
            for(var aResult=0; aResult<s.Data.results.length; aResult++){

                if( (s.Data.results[aResult].model == s.whatModel) && (sessionStorage.getItem("userPath") == s.Data.results[aResult].path) ){

                    var numberOfChoices = s.Data.results[aResult].result.length;

                    for(var thisChoice=0; thisChoice<numberOfChoices; thisChoice++){
                            if (s.ModelsArr.indexOf(s.Data.results[aResult].result[thisChoice]) == -1) {
                            s.ModelsArr.push(s.Data.results[aResult].result[thisChoice]);
                        }
                    }
                }
            }

            // CHECK IF Investor Protection has been picked, and if not, is it in the ModelsArr? 

            var showIP = false;
            var potentiallyShowIP = false;

            for(var i=0; i<s.ChoicesArr.length; i++){
                if (s.Related.indexOf(s.ChoicesArr[i]) != -1) {
                    potentiallyShowIP = false;
                    break;
                }else{
                    potentiallyShowIP = true;
                }
            }

            if(potentiallyShowIP === true){
                for(var i=0; i<s.ModelsArr.length; i++){
                    if (s.Related.indexOf(s.ModelsArr[i]) != -1) {
                        showIP = true;
                        break;
                    }
                }
            }

            s.benefitsArray = [];

            for (var i = 0; i < s.Data.models.length; i++) {
                var obj = s.Data.models[i];
                if (obj.model == s.whatModel) {
                    s.benefitsArray = obj.relatedBenefits;
                }
            }
            
            s.whatsTheDifference = [];
            s.whatsTheDifference = APP.ArrayDifference(s.ChoicesArr,s.ModelsArr);
            s.benefitsNotInModel = false;     
            s.benefitsNotInModel = APP.ArrayDifference(s.benefitsArray,s.ModelsArr);

            for(var i = 0; i<s.benefitsNotInModel.length; i++){
                s.whatsTheDifference.push(s.benefitsNotInModel[i]);
            }

            /* Something to do with if empty array returned BUT ignores that Investor Protection (IP) "clause */

            if( (s.whatsTheDifference.length>0)||( showIP == true)){

                var doINeedAnAnd = function () {
                    noOfDifferences--;
                    if(noOfDifferences>1){
                        otherBenefitsText += ", ";
                    }else if(noOfDifferences == 1){
                        otherBenefitsText += " and ";
                    }
                };

                /* Check which "other benefits" are permitted */

                var showOtherBenefits = false;

                var noOfDifferencesBeforeTrimming = s.whatsTheDifference.length;

                s.amendedWhatsTheDifference = [];

                for (var i = 0; i < noOfDifferencesBeforeTrimming; i++) {
                    if(s.benefitsArray.indexOf(s.whatsTheDifference[i]) != -1){
                        showOtherBenefits = true;
                        if(s.amendedWhatsTheDifference.indexOf(s.whatsTheDifference[i]) === -1){
                            s.amendedWhatsTheDifference.push(s.whatsTheDifference[i]);
                        }
                    }
                }

                var noOfDifferences = s.amendedWhatsTheDifference.length;

                sessionStorage.setItem('whatsTheDifferenceStored',s.amendedWhatsTheDifference);
                sessionStorage.setItem('benefitsArrayStored',s.benefitsArray);

                APP.updateDev();


                if( (showOtherBenefits == true)||(showIP == true) ){

                    var otherBenefitsText = "<h3>Other benefits that can be fully realised include<br />";

                    if (showIP === true ) {
                        // To compensate for no IP model refs in whatsTheDifference
                        noOfDifferences++;

                        if(sessionStorage.userPath == 2) {
                            otherBenefitsText += " <span class=\"reg_block\"></span>market visibility";
                        }else{
                            otherBenefitsText += " <span class=\"reg_block\"></span>investor protection";
                        }
                        doINeedAnAnd();
                    }    

                    if ( (s.amendedWhatsTheDifference.indexOf("a") != -1) && (s.benefitsArray.indexOf("a") != -1) ) {
                        otherBenefitsText += " <span class=\"infra_block\"></span>settlement harmonisation";
                        doINeedAnAnd();
                    }
                    if ( (s.amendedWhatsTheDifference.indexOf("b") != -1) && (s.benefitsArray.indexOf("b") != -1) ) {
                        otherBenefitsText += " <span class=\"infra_block\"></span>Central Bank money";
                        doINeedAnAnd();
                    }
                    if ( (s.amendedWhatsTheDifference.indexOf("c") != -1) && (s.benefitsArray.indexOf("c") != -1) ) {
                        otherBenefitsText += " <span class=\"client_block\"></span>local market expertise";
                        doINeedAnAnd();
                    }
                    if ( (s.amendedWhatsTheDifference.indexOf("d") != -1) && (s.benefitsArray.indexOf("d") != -1) ) {
                        otherBenefitsText += " <span class=\"reg_block\"></span>liquidity optimisation";
                        doINeedAnAnd();
                    }
                    if ( (s.amendedWhatsTheDifference.indexOf("h") != -1) && (s.benefitsArray.indexOf("h") != -1) ) {
                        otherBenefitsText += " <span class=\"reg_block\"></span>asset mobility";
                        doINeedAnAnd();
                    }
                    if ( (s.amendedWhatsTheDifference.indexOf("i") != -1) && (s.benefitsArray.indexOf("i") != -1) ) {
                        otherBenefitsText += " <span class=\"client_block\"></span>best execution";
                        doINeedAnAnd();
                    }
                    if ( (s.amendedWhatsTheDifference.indexOf("j") != -1) && (s.benefitsArray.indexOf("j") != -1) ) {
                        otherBenefitsText += " <span class=\"client_block\"></span>global reach";
                        doINeedAnAnd();
                    }

                    otherBenefitsText += "</h3>";

                    document.getElementById("other-benefits").innerHTML = otherBenefitsText;

                } else {
                    if (showIP === true ) {
                        document.getElementById("other-benefits").innerHTML = "<h3>Other benefits that can be fully realised include <span class=\"reg_block\"></span>investor protection</h3>";
                    }    
                }
            }

            var modelChoices = "";

            var devChoiceList = [];

            var counting = 0;
 
            $.each(s.ChoicesArr, function(index, val) {            
                
                var result = $.grep(s.Data.choices, function(e) {
                    return e.choice == val;
                });

                /* Highlight the chosen options */
                var choice = $('.component[data-choice-ref=' + result[0].choice + ']');

                // $(choice).addClass('component--priority');

                /* Populate the hidden form field with the chosen options */

                var descriptionResult = $.grep(s.Data.textForDiagrams, function(e) {
                    return e.pickedChoice == result[0].choice;
                });

                for(var i = 0; i < descriptionResult.length; i++){

                    var acceptableModelsArray = descriptionResult[i].pickedModels;

                    s.whatModel = s.whatModel.toString();
                    
                    if(acceptableModelsArray.indexOf(s.whatModel) != -1){
                            choice.find("p").text(descriptionResult[i].description);
                            break;
                        }
                }
                        
                modelChoices = modelChoices + result[0].name + ', ';  
                var whatToPush = "[" + s.ChoicesArr[counting] + "] " + result[0].name;
 
            });

        } else {
            
            /* No choices made and therefore the selection page has been bypassed */
            modelChoices = 'No options selected';

        }

        sessionStorage.storedDevChoiceList = devChoiceList;
        sessionStorage.storedModelTitle = modelTitle;
        sessionStorage.storedModelChoices = modelChoices; 
        sessionStorage.stroredModelsArray = s.ChoicesArr;

        APP.updateDev();

        for(var i = 0; i < s.ChoicesArr.length; i++){

                // sessionStorage.stroredModelsArray are the selections in the order selected;

                var devChoiceList = $('.component[data-choice-ref=' + s.ChoicesArr[i] + ']');
                if (devChoiceList.hasClass("component--reg")){
                    $(".component-option-"+i).addClass("component--reg");
                }else if (devChoiceList.hasClass("component--infra")){
                    $(".component-option-"+i).addClass("component--infra");
                }else if (devChoiceList.hasClass("component--client")){
                    $(".component-option-"+i).addClass("component--client");
                }

                var theChoiceRef = devChoiceList.data("choice-ref");
                $(".component-option-"+i).data('choice-rereffed',theChoiceRef);

                devChoiceList = devChoiceList.html();
                $(".component-option-"+i).html(devChoiceList);

            
            }
      
        /* Init Wufoo form */

        /*
        s.WufooHash = new window.WufooForm();
        s.WufooHash.initialize({
        'userName':'wardour',
        'formHash':'qaqgqab1m5t8we',
        'autoResize':true,
        'height':'450',
        'async':true,
        'host':'wufoo.com',
        'header':'show',
        'defaultValues':'Field7='+modelTitle+'&Field8='+modelChoices+'&Field10='+sessionStorage.getItem('areaOfBusiness'),
        'ssl':false});

        s.WufooHash.display();
        */
    },

    RemoveFrom: function(val) {

        /* Remove previous data value from the array */
        s.
        sArr.splice($.inArray(val, s.ChoicesArr),1);

        /* Reset state of the original */
        $('.options').find('[data-option='+ val +']').removeClass(c.Disabled);

    },

    Router: function() {

        /* Configure smoothState to handle dynamic page loading */
        var options = {
            prefetch: true,
            cacheLength: 15,
            scroll: false,
            debug: true,
            anchors: 'a',
            onStart: {
                duration: 500,
                render: function ($container) {
                    $container.addClass('is-exiting');
                    
                    /* This forces a restart of the animations, jquery will auto prefix for us */
                    var els = $('.animate');
                    $.each(els,function(i,el) {
                        
                        var animation = $(el).css('animation-name');
                        $(el).css('animation-name', 'none');
                        $(el).height();
                        $(el).css({'animation-name':animation, 'animation-delay':'0ms'});

                    });

                }
            },
            onReady: {
                duration: 0,
                render: function ($container, $newContent) {
                    $container.removeClass('is-exiting');
                    $container.html($newContent);
                }
            },
            onAfter: function(){

                APP.updateDev();

                /* Rebind the necessary UI events */
                APP.BindUI();

                /* Prepare the form data */
                if($(s.ModelGenerator).length) {
                    APP.PrepareForm();
                }

                /* Clear the choices array */
                if(s.ChoicesArr.length){
                    // APP.ClearChoices();
                }

                if($(s.Issues).length){
                    APP.ClearChoices();
                    APP.ClearModels();
                }

                // Functions for the "What is your primary area of focus" Customise page 

                if (sessionStorage.getItem("userPath") !== null){


                    if ($(".customise").length) {

                    // Tick Customise Value
                    var theAreaOfBusiness = sessionStorage.getItem('areaOfBusiness');

                        $(".customise--radio").each(function(){
                            if($(this).attr("value") == theAreaOfBusiness) {
                                $(this).click();
                                return false;
                            }
                        });
                    } 

                    if ( ($("#model__generator").length) && (sessionStorage.userPath == 2) ) {
                       $(".new-components").each(function(){
                            if (s.Related.indexOf($(this).data("choice-rereffed")) != -1){
                                var thisNumber = $(this).find("h3").text();
                                thisNumber = thisNumber.split("(").pop();
                                $(this).find("h3").text("Market visibility ("+thisNumber);                              
                            }

                        });
                    }

                }   


                if ( ($(".issues").length) && (sessionStorage.userPath == 2) ) {
                   $(".options__item").each(function(){
                        if (s.Related.indexOf($(this).data("option")) != -1){
                            var thisNumber = $(this).find(".module__title").text();
                            thisNumber = thisNumber.split("(").pop();
                            $(this).find(".module__title").text("Market visibility ("+thisNumber);                              
                        }

                    });
                }

                if($(".map").length){
                    $(".bc_where").addClass("activated");
                }

                if($("#model__generator").length){
                    $(".bc_solution").addClass("activated");
                }

                if($(".ava").length){
                    $(".bc_otherpdcts").addClass("activated");
                    $(".bc_otherpdcts_multi").addClass("activated");
                }

                $(".bc_where").attr("href", "../maps/map"+s.whatModel+".html");  

                $(".bc_solution").attr("href", "../models/model"+s.whatModel+".html");   


                /* Other Products Section - START */

                if( (s.otherProducts == null)||(s.otherProducts.length <= 0) ) {

                    $(".bc_otherpdcts").css("display", "none");

                }else if (s.otherProducts.length == 1){

                    var otherProductResult = $.grep(s.Data.otherproducts, function(e) {
                        return e.product == s.otherProducts[0];
                    });

                    $(".bc_otherpdcts").css("display", "inline-block");

                    $(".bc_otherpdcts").attr("href", "../"+otherProductResult[0].href);

                    if($(".map").length){
                        $(".app__nav__item--next").attr("href", "../"+otherProductResult[0].href); 
                    }

                    if($(".ava").length){
                        $(".app__nav__item--back").attr("href", "../maps/map"+s.whatModel+".html");  
                    }

                }else if (s.otherProducts.length > 1){

                    $(".bc_otherpdcts").css("display", "none");

                    $(".bc_otherpdcts_multi").css("display", "inline-block");

                    var otherProductResult = $.grep(s.Data.otherproducts, function(e) {
                        return e.product == s.otherProducts[0];
                    });

                    if($(".map").length){
                        $(".app__nav__item--next").attr("href", "../"+otherProductResult[0].href); 
                    }
                    
                    if($(".ava").length){

                        var whatAVAarewe =  $(".ava").data("productref");

                        for(var i = 0; i<s.otherProducts.length; i++){
     
                            if(s.otherProducts[i] !== whatAVAarewe){

                                var getIndex = s.otherProducts.indexOf(whatAVAarewe);

                                var anotherProductResult = $.grep(s.Data.otherproducts, function(e) {
                                    return e.product == s.otherProducts[1];
                                });

                                if(getIndex==0){

                                    $(".app__nav__item--back").attr("href", "../maps/map"+s.whatModel+".html");  
                                    $(".app__nav__item--next").attr("href", "../"+anotherProductResult[0].href); 
                                }else{
                                   $(".bc_otherpdcts_multi").css("background-image", "url(../img/bc_otherpdcts_multi22.png)");
                                   $(".app__nav__item--back").attr("href", "../"+otherProductResult[0].href);       
                                    $(".app__nav__item--next").attr("href", "#signup"); 
                                }

                                var otherProductResult = $.grep(s.Data.otherproducts, function(e) {
                                    return e.product == s.otherProducts[i];
                                });

                                $(".bc_otherpdcts_multi").attr("href", "../"+otherProductResult[0].href);

                            }      
                        }

                    }else{

                        $(".bc_otherpdcts_multi").attr("href", "../"+otherProductResult[0].href);
                    }
                }

                /* Other Products Section - END */

            }
        },
        smoothState = s.Main.smoothState(options).data('smoothState');
    },

    updateDev: function(){

       $("#dev_up").text("User Path: "+sessionStorage.userPath+" -> Model No.: "+s.whatModel+" -> Result No.: "+s.modelResultNumber);

        var thedevChoiceList;

        if (sessionStorage.getItem("storedDevChoiceList") !== null) {
            thedevChoiceList = sessionStorage.storedDevChoiceList;        
        }else{
            thedevChoiceList = "Not yet chosen";
        }
            
        $("#dev_ic").text("Issues Chosen: "+sessionStorage.storedModelChoices);
        $("#dev_ma").text("Models Array: "+sessionStorage.stroredModelsArray);    
        $("#dev_opa").text("Benefits Array: "+s.benefitsArray);   
        $("#dev_wtd").text("What's Difference Array: "+s.whatsTheDifference);   
        $("#dev_awd").text("Amended What's Difference Array: "+s.amendedWhatsTheDifference);   
    }
};

APP.Init();